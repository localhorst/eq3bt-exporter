#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
    Date of creation:           2022/12/17
    Date of last modification:  2023/01/08
"""

from http.server import BaseHTTPRequestHandler, HTTPServer
import time
import threading
from datetime import datetime
from urllib.parse import urlsplit, parse_qs
from random import randrange
from eq3bt import Thermostat

hostName = "10.10.3.6"
serverPort = 9101
exporter_prefix = "eq3bt_"
eq3bt_mac = "00:1A:22:0F:20:CA"

request_count = 0
scrape_healthy = True
startTime = datetime.now()
eq3bt_metrics = list()
mutex = threading.Lock()

thermostat = Thermostat(eq3bt_mac)

class RequestHandler(BaseHTTPRequestHandler):

    def get_metrics(self):
        global request_count
        global eq3bt_metrics
        global exporter_prefix
        global mutex
        mutex.acquire()
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes(exporter_prefix + "expoter_duration_seconds_sum " + str(int((datetime.now() - startTime).total_seconds())) + "\n", "utf-8"))
        self.wfile.write(bytes(exporter_prefix + "exporter_request_count " + str(request_count) + "\n", "utf-8"))
        self.wfile.write(bytes(exporter_prefix + "exporter_scrape_healthy " + str(int(scrape_healthy)) + "\n", "utf-8"))
        
        for metric in eq3bt_metrics:
            #print(metric)
            self.wfile.write(bytes(exporter_prefix + metric + "\n", "utf-8"))

        mutex.release()
        
    def do_GET(self):
        global request_count
        request_count = request_count + 1
        print("Request: " + self.path)
        if (self.path.startswith("/metrics")): 
            self.get_metrics()
        else:
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes("<html>", "utf-8"))
            self.wfile.write(bytes("<head><title>eq3bt exporter</title></head>", "utf-8"))
            self.wfile.write(bytes("<body>", "utf-8"))
            self.wfile.write(bytes('<h1>eq3bt exporter based on data from <a href="https://github.com/rytilahti/python-eq3bt/">https://github.com/rytilahti/python-eq3bt/</a></h1>', "utf-8"))
            self.wfile.write(bytes('<p><a href="/metrics">Metrics</a></p>', "utf-8"))
            self.wfile.write(bytes("</body>", "utf-8"))
            self.wfile.write(bytes("</html>", "utf-8"))


def update_metrics():
    while True:
        print("poll data from eq3bt")
        global eq3bt_metrics
        global mutex
        global scrape_healthy
        mutex.acquire()
        scrape_healthy = True
        eq3bt_metrics.clear()

        try:
            thermostat.update() 
            eq3bt_metrics.append("target_temperature " + str(int(thermostat.target_temperature)))
            eq3bt_metrics.append("valve " + str(int(thermostat.valve_state)))
            eq3bt_metrics.append("low_battery " + str(int(thermostat.low_battery)))
            eq3bt_metrics.append("window_open " + str(int(thermostat.window_open)))
        except Exception as ex:
            print("unable to poll data from eq3bt! error: " + str(ex))
            scrape_healthy = False
            pass
        mutex.release()
        time.sleep(300)


def main():
    print("start")

    webServer = HTTPServer((hostName, serverPort), RequestHandler)

    print("Server started http://%s:%s" % (hostName, serverPort))

    update_metrics_thread = threading.Thread(target=update_metrics, args=())
    update_metrics_thread.start()

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
    update_metrics_thread.join()

if __name__ == "__main__":
    main()
