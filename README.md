# eq3bt-exporter

Scrapes [EQ3](https://www.eq-3.com/products/eqiva/detail/bluetooth-smart-radiator-thermostat.html) bluetooth device and export as prometheus metric.

## Requirements

- `git clone https://github.com/rytilahti/python-eq3bt`
- `cd python-eq3bt`
- `pip install .`
- Pair the eq3bt as shown in the [python-eq3bt README](https://github.com/rytilahti/python-eq3bt)

## Features and Limitations
- Polls data ever 5 minutes from the EQ3
- Caches the data for faster prometheus crapes
- Only suppoting **one** eq3bt device at this dev stage

## Config 

Set your MAC in `eq3bt-exporter.py`.

## Usage 

to start: `python3 q3bt-exporter.py`

to get metrics: `curl http://127.0.0.1:9100/metrics`

Example metrics:

```
eq3bt_expoter_duration_seconds_sum 3 
eq3bt_exporter_request_count 9 
eq3bt_exporter_scrape_healthy 1 
eq3bt_target_temperature 12 
eq3bt_valve 12 
eq3bt_low_battery 0 
eq3bt_window_open 0
```

## Permanent Install


`cd /opt/`

`git clone https://git.mosad.xyz/localhorst/eq3bt-exporter`

`cd eq3bt-exporter`

Change host/port in `q3bt-exporter.py`

`mv eq3bt-exporter.service /usr/lib/systemd/system/`

`systemctl daemon-reload`

`systemctl enable --now /usr/lib/systemd/system/eq3bt-exporter.service`

`systemctl status eq3bt-exporter.service`
